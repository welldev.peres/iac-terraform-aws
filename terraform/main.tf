terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = var.region
}

resource "aws_instance" "app_server" {
  ami           = var.ami
  instance_type = var.tipo_maquina

  tags = {
    Name = var.name_instance
  }
}

# Declaração do recurso de dados para a VPC padrão
data "aws_vpc" "default" {
  default = true
}

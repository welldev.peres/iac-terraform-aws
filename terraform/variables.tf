variable "region" {
  description = "The AWS region to deploy resources"
  type        = string
  default     = "us-east-1"
}

variable "tipo_maquina" {
  description = "Tipo da instancia (t2-Micro)"
  type        = string
  default     = "t2.micro"
}

variable "ami" {
  description = "AMI default free tier t2 Micro"
  type        = string
  default     = "ami-04b70fa74e45c3917"
}

variable "name_instance" {
  description = "Nome da Instancia"
  type        = string
  default     = "instance-test"
}

variable "name_firewall" {
  description = "Nome do Security Group"
  type        = string
  default     = "my_sg"
}

variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnet_1_cidr" {
  description = "CIDR block for the first public subnet"
  type        = string
  default     = "10.0.1.0/24"
}

variable "public_subnet_2_cidr" {
  description = "CIDR block for the second public subnet"
  type        = string
  default     = "10.0.2.0/24"
}
